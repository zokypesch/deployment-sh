#!/bin/sh
echo "please put your docker image, example: {username}/{repository}:latest"
read docker_image
sudo apt-get update
apt-get -y install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable"
apt-get update
apt-get -y install docker-ce
sysctl -w net.ipv4.conf.docker0.route_localnet=1

iptables -t nat -I PREROUTING -i docker0 -d 172.17.0.1 -p tcp --dport 6379 -j DNAT --to 127.0.0.1:6379
iptables -t filter -I INPUT -i docker0 -d 127.0.0.1 -p tcp --dport 6379 -j ACCEPT

docker pull $docker_image
